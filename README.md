**Seminární projekt:**
- Jedná se o klasickou hru "had" v programovacím jazyce Python v programu VS Code.

**Cíle:** 
- vytvořit samotnou hru podle [tutoriálu](https://www.youtube.com/watch?v=QFvqStqPCRU&t=6787s) - 🎉hotovo🎉
- .py -> .exe
- ostatní body přístě :P

**Použité zdroje:**
- https://www.youtube.com/watch?v=QFvqStqPCRU&t=6787s
- https://www.youtube.com/watch?v=FfWpgLFMI7w
- tutotirál .py -> .exe


[Prezentace](https://docs.google.com/presentation/d/1Bxqce4JUgvtuAJCgDlWC6VH8G2ugCqnO1Ky6GNecAx4/edit?usp=sharing)
